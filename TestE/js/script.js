$(document).ready(function() {
    $('#loginForm').submit(function(event) {
        event.preventDefault();

        var formData = {
            email: $('#email1').val(),
            password: $('#password1').val()
        };

        $.ajax({
            type: 'POST',
            url: 'http://localhost:9100/auth/login',
            contentType: 'application/json',
            data: JSON.stringify(formData),
            success: function(response) {
                alert('Login successful');

                // Lưu token vào Local Storage
                localStorage.setItem('jwt', response.token);

                // Chuyển hướng sang trang display.html
                window.location.href = 'display.html';
            },
            error: function(xhr, status, error) {
                alert('Login failed!');
                console.error('Error:', error);
            }
        });
    });
});


    $(document).ready(function() {
    $('#registerForm').submit(function(event) {
        event.preventDefault();


        var formData = {
            email: $('#email').val(),
            password: $('#password').val(),
            retypePassword: $('#confirm_password').val()
        };

        $.ajax({
            type: 'POST',
            url: 'http://localhost:9100/auth/register-user',
            contentType: 'application/json',
            data: JSON.stringify(formData),
            success: function(response) {
                // Handle success response
                $('#message').html('Đăng ký thành công!');
            },
            error: function(xhr, status, error) {
                // Handle error response
                var errorMessage = xhr.responseJSON.message;
                $('#message').html('Đăng ký thất bại: ' + errorMessage);
            }
        });
    });
});

$(document).ready(function() {
    // Function to get all books data and display in table
    function getAllBooks() {
        $.ajax({
            type: 'GET',
            url: 'http://localhost:9100/book/list',
            success: function(response) {
                // Handle success response
                displayBooks(response);
            },
            error: function(xhr, status, error) {
                // Handle error response
                var errorMessage = xhr.responseJSON.message;
                alert('Lỗi: ' + errorMessage);
            }
        });
    }

    // Function to display books data in table
    function displayBooks(books) {
        var bookTableBody = $('#bookTableBody');
        bookTableBody.empty();
        $.each(books, function(index, book) {
            // Append book information to the table
            bookTableBody.append(
                '<tr>' +
                '<td>' + book.idBook + '</td>' +
                '<td>' + book.bookName + '</td>' +
                '<td>' + book.authorName + '</td>' +
                '<td>' + book.categoryBook + '</td>' +
                '<td>' + book.publishYear + '</td>' +
                '<td>' + book.publishCompany + '</td>' +
                '<td>' + (book.description ? book.description : '') + '</td>' +
                '<td>' + book.priceBook + '</td>' +
                '<td><a href="#" class="link-dark edit-book" data-id="' + book.idBook + '"><i class="fa-solid fa-pen-to-square fs-5 me-3"></i></a></td>' +
                '</tr>'
            );
        });

        // Add click event for edit buttons
        $('.edit-book').click(function() {
            // Get the book ID from data-id attribute
            var bookId = $(this).data('id');
            // Redirect to edit.html with book ID as query parameter
            window.location.href = 'edit.html?id=' + bookId;
        });
    }

    // Call the function to get all books data
    getAllBooks();
});

$(document).ready(function() {
    $('#addBookForm').submit(function(event) {
        event.preventDefault(); // Prevent default form submission

        // Get form data
        var formData = {
            bookName: $('#bookName').val(),
            authorName: $('#authorName').val(),
            categoryBook: $('#categoryBook').val(),
            publishYear: $('#publishYear').val(),
            publishCompany: $('#publishCompany').val(),
            description: $('#description').val(),
            priceBook: $('#priceBook').val()
        };

        // Get JWT token from localStorage
        var token = localStorage.getItem('jwt');

        // Send POST request to add book
        $.ajax({
            type: 'POST',
            url: 'http://localhost:9100/book/add',
            contentType: 'application/json',
            headers: {
                'Authorization': 'Bearer ' + token // Include JWT token in request headers
            },
            data: JSON.stringify(formData),
            success: function(response) {
                // Handle success response
                alert('Thêm sách thành công!');
                // Redirect to home page or other page
                window.location.href = 'display.html'; // Change to the desired page
            },
            error: function(xhr, status, error) {
                // Handle error response
                var errorMessage = xhr.responseJSON.message;
                alert('Thêm sách thất bại: ' + errorMessage);
            }
        });
    });
});

$(document).ready(function() {
    var urlParams = new URLSearchParams(window.location.search);
    var bookId = urlParams.get('id');
    document.getElementById("idBook").value = bookId;
    $('#editBookForm').submit(function(event) {
        event.preventDefault(); 

        var formData = {
            idBook: $('#idBook').val(),
            bookName: $('#bookName').val(),
            authorName: $('#authorName').val(),
            categoryBook: $('#categoryBook').val(),
            publishCompany: $('#publishCompany').val(),
            description: $('#description').val(),
            priceBook: $('#priceBook').val(),
            publishYear: $('#publishYear').val()
        };

        // Lấy mã JWT từ localStorage
        var token = localStorage.getItem('jwt');

        // Gửi yêu cầu cập nhật sách đến API
        $.ajax({
            type: 'PUT',
            url: 'http://localhost:9100/book/update',
            contentType: 'application/json',
            headers: {
                'Authorization': 'Bearer ' + token // Bao gồm mã JWT trong tiêu đề yêu cầu
            },
            data: JSON.stringify(formData),
            success: function(response) {
                alert('Cập nhật thông tin sách thành công!');
                window.location.href = 'display.html'; // Chuyển hướng đến trang hiển thị sách sau khi cập nhật thành công
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.responseJSON.message;
                alert('Lỗi khi cập nhật sách: ' + errorMessage);
            }
        });
    });
});